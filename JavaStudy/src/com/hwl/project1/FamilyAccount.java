package com.hwl.project1;

/**
 * @author 何文亮
 * @date 2021-01-12
 */
public class FamilyAccount {
    public static void main(String[] args) {
        boolean isFlag = true;
        //用于记录收入详情
        String details = "收支\t账户金额\t收支金额\t说  明\n";
        int blance = 10000;

        while (isFlag) {
            System.out.println("----------家庭收支记账软件----------\n");
            System.out.println("           1.收支明细");
            System.out.println("           2.登记收入");
            System.out.println("           3.登记支出");
            System.out.println("           4.退出系统\n");
            System.out.print("             请选择（1-4）：");
            char selection = Utility.readMenuSelection();
            switch (selection) {
                case '1':
                    System.out.println("--------------当前收支明细记录--------------");
                    System.out.println(details);
                    System.out.println("------------------------------------------");
                    break;
                case '2':
                    System.out.print("本次收入金额：");
                    int addmoney = Utility.readNumber();
                    System.out.print("本次收入说明：");
                    String addinfo = Utility.readString();
                    blance += addmoney;
                    details += ("收入\t" + blance + "\t" + addmoney + "\t\t" + addinfo + "\n");
                    System.out.println("-----------------登记完成-----------------\n");
                    break;
                case '3':
                    System.out.print("本次支出金额：");
                    int minusmoney = Utility.readNumber();
                    if (blance >= minusmoney) {
                        System.out.print("本次支出说明：");
                        String minusinfo = Utility.readString();
                        blance -= minusmoney;
                        details += ("支出\t" + blance + "\t" + minusmoney + "\t\t" + minusinfo + "\n");
                        System.out.println("-----------------登记完成-----------------\n");
                    } else {
                        System.out.println("请不要超出余额！！");
                    }
                    break;
                case '4':
                    System.out.println("是否确认退出（Y/N）：");
                    char isExit = Utility.readConfirmSelection();
                    if (isExit == 'Y') {
                        isFlag = false;
                    }
                    break;
                default:
            }
        }
    }
}