package com.hwl.basic;

import java.util.Scanner;

/**
 * @author 何文亮
 * @date 2021-01-09
 */
public class iFText1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("请输入您的成绩：");
        int score = in.nextInt();
        if (score == 100) {
            System.out.println("BWM");
        } else if (score > 80 && score <= 99) {
            System.out.println("Iphone XR");
        }else if(score > 60 && score <= 80 ){
            System.out.println("Ipad");
        }else{
            System.out.println("Nothing To You!");
        }
    }

}
