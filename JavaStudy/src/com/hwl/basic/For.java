package com.hwl.basic;

/**
 * @author 何文亮
 * @date 2021-01-11
 */
public class For {
    public static void main(String[] args) {
        int sum = 0, count = 0;
        for (int i = 1; i <= 100; i++) {
            if (i % 2 == 0) {
                System.out.println(i);
                sum += i;
                count++;
            }
        }
        System.out.println("100以内有" + count + "个偶数");
        System.out.println("100以内所有偶数之和为：" + sum);
    }
}
