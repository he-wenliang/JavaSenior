package com.hwl.basic;

import java.util.Scanner;

/**
 * @author 何文亮
 * @date 2021-01-11
 */
public class For2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入两个数：");
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int min = m < n ? m : n;
        for (int i = min; i >= 1; i--) {
            if (m % i == 0 && n % i == 0) {
                System.out.println("最大公约数" + i);
//                System.out.println("最小公倍数" + (i * (m / i) * (n / i)));
                break;
            }
        }
        int max = m >= n ? m : n;
        for (int i = max; i <= m * n; i++) {
            if (i % m == 0 && i % n == 0) {
                System.out.println("最小公倍数" + i);
                break;
            }
        }
    }
}
