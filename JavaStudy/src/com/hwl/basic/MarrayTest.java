package com.hwl.basic;

import java.util.Scanner;

/**
 * @author 何文亮
 * @date 2021-01-10
 */
public class MarrayTest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入您的身高：（cm）");
        int hight = scanner.nextInt();
        System.out.println("请输入您的资产：（千万）");
        double many = scanner.nextDouble();
        /*方式一：
        System.out.println("您是否长的帅：（true/false）(是/否)");
        boolean face = scanner.nextBoolean();
        if (hight >= 180 && many >= 1 && face == true) {
            System.out.println("我一定要嫁给他~~");
        } else if (hight >= 180 || many >= 1 || face == true) {
            System.out.println("嫁吧，比上不足比下有余~");
        } else {
            System.out.println("不嫁！");
        }
         */
//        方式二：
        System.out.println("您是否长的帅：(是/否)");
        String str = scanner.nextLine();
        if (hight >= 180 && many >= 1 && (str.equals("是"))) {
            System.out.println("我一定要嫁给他~~");
        } else if (hight >= 180 || many >= 1 || (str.equals("是"))) {
            System.out.println("嫁吧，比上不足比下有余~");
        } else {
            System.out.println("不嫁！");
        }
    }
}
