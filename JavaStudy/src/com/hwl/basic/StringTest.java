package com.hwl.basic;

/**
 * @author 何文亮
 * @date 2021-01-07
 */
public class StringTest {
    public static void main(String[] args) {
        String s1 = "我说今晚月光那么美，";
        String s2 = "你说是的！";
        //+：表示连接符
        System.out.println(s1+s2);
        String str = 3.5F + "";
        System.out.println(str);
    }
}
