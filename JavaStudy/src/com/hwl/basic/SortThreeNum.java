package com.hwl.basic;

import java.util.Scanner;

/**
 * @author 何文亮
 * @date 2021-01-10
 */
public class SortThreeNum {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入三个数：");
        int num1 = scanner.nextInt();
        int num2 = scanner.nextInt();
        int num3 = scanner.nextInt();
        if (num1 > num2) {
            if (num3 >= num1) {
                System.out.println("从大到小排序：" + num3 + "," + num1 + "," + num2);
            } else if(num3 <= num2 ){
                System.out.println("从大到小排序：" + num1 + "," + num2 + "," + num3);
            }else{
                System.out.println("从大到小排序：" + num1 + "," + num3 + "," + num2);
            }
        } else{
            if (num3 >= num2) {
                System.out.println("从大到小排序：" + num3 + "," + num2 + "," + num1);
            } else if(num3 <= num1){
                System.out.println("从大到小排序：" + num2 + "," + num1 + "," + num3);
            }else{
                System.out.println("从大到小排序：" + num2 + "," + num3 + "," + num1);
            }
        }
    }
}
