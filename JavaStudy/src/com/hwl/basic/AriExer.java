package com.hwl.basic;

import java.util.Scanner;

/**
 * @author 何文亮
 * @date 2021-01-08
 */
public class AriExer {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("请输入一个三位数：");
        int num = in.nextInt();
        System.out.println("这个数的百位是：" + num / 100);
        System.out.println("这个数的十位是：" + (num / 10)%10);
        System.out.println("这个数的个位是：" + num % 10);
        int n = 10;
        n += (n++) + (++n);
        System.out.println(n);
        System.out.println(4>3);
        int num1 = 23,num2 = 54,temp;
        temp = num1;
        num1 = num2;
        num2 = temp;
//        num1 = num1 ^ num2;
//        num2 = num1 ^ num2;
//        num1 = num1 ^ num2;
        System.out.println("numb1 = " + num1 + ",numb2 = " + num2);
    }
}
