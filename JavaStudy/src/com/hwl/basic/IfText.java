package com.hwl.basic;

/**
 * @author 何文亮
 * @date 2021-01-09
 */
public class IfText {
    public static void main(String[] args) {
        int num1 = 13, num2 = 5, num3 = 53, max;
        if(num1 >= num2 && num1 >= num3){
            max = num1;
        }else if(num2 >= num1 && num2 >= num3){
            max = num2;
        }else{
            max = num3;
        }
        System.out.println(max);
        String str1 = Integer.toBinaryString(60);
        String str2 = Integer.toHexString(60);
        System.out.println(str1);
        System.out.println(str2);
    }
}
