package com.hwl.basic;

import java.util.Scanner;

/**
 * @author 何文亮
 * @date 2021-01-10
 */
public class CaiPiaoTest {
    public static void main(String[] args) {
        int num = (int) (Math.random() * 90 + 10);
        int numshi = num / 10;
        int numge = num % 10;
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入您的彩票号码：");
        int input = scanner.nextInt();
        int inputshi = input / 10;
        int inputge = input % 10;
        if (num == input) {
            System.out.println("奖金10000");
        } else if (numshi == inputge && numge == inputshi) {
            System.out.println("奖金3000");
        } else if (numshi == inputshi || numge == inputge) {
            System.out.println("奖金1000");
        } else if (numshi == inputge || numge == inputshi) {
            System.out.println("奖金500");
        } else {
            System.out.println("没有中奖");
        }
        System.out.println("中奖的彩票号码为" + num);
    }

}
