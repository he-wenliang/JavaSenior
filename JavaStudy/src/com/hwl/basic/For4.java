package com.hwl.basic;

/**
 * @author 何文亮
 * @date 2021-01-11
 */
public class For4 {
    public static void main(String[] args) {
        for (int j = 0; j < 4; j++) {
            for (int i = 0; i <= j; i++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
