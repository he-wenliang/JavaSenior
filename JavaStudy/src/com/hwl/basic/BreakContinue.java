package com.hwl.basic;

/**
 * @author 何文亮
 * @date 2021-01-11
 */
public class BreakContinue {
    public static void main(String[] args) {
        for (int i = 1; i < 10; i++) {
            if (i % 4 == 0) {
//                break;
                continue;
            }
            System.out.println(i);
        }
        //************************************
        label:
        for (int i = 1; i <= 4; i++) {
            for (int j = 0; j <= 10; j++) {
                if (j % 4 == 0) {
//                    break;
//                continue;
                    break label;
                }
                System.out.print(j);
            }
        }
        System.out.println();
    }
}
