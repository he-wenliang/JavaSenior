package com.hwl.basic;

/**
 * @author 何文亮
 * @date 2021-01-12
 */
public class PrimeNumber1 {
    public static void main(String[] args) {
        int count = 0;
        long start = System.currentTimeMillis();

        label:
        for (int i = 2; i <= 100000; i++) {

            for (int j = 2; j <= Math.sqrt(i); j++) {
                if (i % j == 0) {
                    continue label;
                }
            }
            count++;
        }
        long end = System.currentTimeMillis();
        System.out.println("质数的个数为：" + count);
        System.out.println("时间为" + (end - start));
    }
}
