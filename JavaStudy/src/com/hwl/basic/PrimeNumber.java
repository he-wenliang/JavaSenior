package com.hwl.basic;

/**
 * @author 何文亮
 * @date 2021-01-11
 */
public class PrimeNumber {
    //质数：素数，只能被1和它本身整除的自然数
    public static void main(String[] args) {
        boolean isFlag = true;
        long start = System.currentTimeMillis();

        for (int i = 2; i < 100; i++) {
            //优化二 j <= Math.sqrt(i);
            for (int j = 2; j <= Math.sqrt(i); j++) {
                if (i % j == 0) {
                    isFlag = false;
                    //优化一
                    break;
                }
            }
            if (isFlag == true) {
                System.out.println(i);
            }
            isFlag = true;
        }
    }
}
