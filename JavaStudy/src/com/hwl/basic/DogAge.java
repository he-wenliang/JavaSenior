package com.hwl.basic;

import java.util.Scanner;

/**
 * @author 何文亮
 * @date 2021-01-10
 */
public class DogAge {
    public static void main(String[] args) {
//        产生一个随机数 [0.0,1.0)
//        (int)(Math.random()*(b-a+1)+a) ---> [a,b]
//        System.out.println(Math.random());
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入狗的年龄：（0-50）");
        int age = scanner.nextInt();
        if (age <= 2 && age > 0) {
            System.out.println("够的年龄为：" + (10.5 * age));
        } else if (age > 2) {
            System.out.println("狗的年龄为：" + (int) (10.5 * 2 + 4 * (age - 2)) + "岁");
        } else {
            System.out.println("输入不合法~~");
        }

    }

}
