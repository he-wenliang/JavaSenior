package com.hwl.basic;

/**
 * @author 何文亮
 * @date 2021-01-11
 */
public class For3 {
    public static void main(String[] args) {
//三位数水仙花数
        for (int i = 100; i < 1000; i++) {
            int bei = i / 100, shi = (i % 100) / 10, ge = i % 10;
            if (i == bei * bei * bei + shi * shi * shi + ge * ge * ge) {
                System.out.println(i);
            }
        }
    }
}
