package com.hwl.basic;

import java.util.Scanner;

/**
 * @author 何文亮
 * @date 2021-01-11
 */
public class ForWhile {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //记录正数个数
        int positiveNumber = 0;
        //记录负数个数
        int negativeNumber = 0;
        //for(;;)
        while (true) {
            int num = scanner.nextInt();
            if (num > 0) {
                positiveNumber++;
            } else if (num < 0) {
                negativeNumber++;
            } else {
                break;
            }
        }
        System.out.println("输入的正数个数为：" + positiveNumber);
        System.out.println("输入的负数数个数为：" + negativeNumber);
    }
}
