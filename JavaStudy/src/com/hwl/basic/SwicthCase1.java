package com.hwl.basic;

import java.util.Scanner;

/**
 * @author 何文亮
 * @date 2021-01-10
 */
public class SwicthCase1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入2019年的月份：");
        int month = scanner.nextInt();
        System.out.println("请输入" + month + "月份的号数：");
        int day = scanner.nextInt();
        int sumdays = 0;
        switch (month) {
            case 12:
                sumdays += 30;
            case 11:
                sumdays += 31;
            case 10:
                sumdays += 30;
            case 9:
                sumdays += 31;
            case 8:
                sumdays += 31;
            case 7:
                sumdays += 30;
            case 6:
                sumdays += 31;
            case 5:
                sumdays += 30;
            case 4:
                sumdays += 31;
            case 3:
                sumdays += 28;
            case 2:
                sumdays += 31;
            case 1:
                sumdays += day;
        }
        System.out.println("2019年的"+month+"月"+day+"日是当年的第"+sumdays+"天");
    }
}
