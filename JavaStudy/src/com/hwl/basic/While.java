package com.hwl.basic;

import java.util.Scanner;

/**
 * @author 何文亮
 * @date 2021-01-11
 */
public class While {
    public static void main(String[] args) {
        //Scanner scanner = new Scanner(System.in);
        int num = 1;
        while (num < 100) {
            if (num % 2 == 0) {
                System.out.println(num);
            }
            num++;
        }
    }
}

