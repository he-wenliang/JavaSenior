package com.hwl.basic;

import java.util.Scanner;

/**
 * @author 何文亮
 * @date 2021-01-11
 */
public class For5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        for (int i = 1; i <= num; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(i + "x" + j + "=" + i * j + "\t");
            }
            System.out.println();
        }
    }
}
