package com.hwl.basic;

/**
 * @author 何文亮
 * @date 2020-12-26
 */
public class HelloWorld {
        /*
        多行注释
        */

    public static void main(String[] args) {
        //单行注释
        System.out.println("Hello，World.");
        byte b1 = 13,b2 = -14;
        System.out.println(b1);
        //整型
        byte b = 24;
        int i = 3;
        short s = 4;
        long l = 3143532L;
        //浮点型 float4字节 double 8字节
        float f = 2.4F;
        double d = 4431.56;
        //char型
        char c1 = '月';
        System.out.println(c1);
        int i1 = (int)d;

    }
}
