package com.hwl.basic;

/**
 * @author 何文亮
 * @date 2021-01-07
 */
public class AriTest {
    public static void main(String[] args) {
        int i1 = 14, i2 = 5, i3, i4;
        float f1 = 14.6F, f2 = 5.3F;
        System.out.println(i1 / i2);
        System.out.println(i1 % i2);
        System.out.println(f1 / i2);
        System.out.println(f1 / f2);
        int b1 = 10;
        i3 = ++b1;
        System.out.println("b1 = " + b1 + ",i3 = " + i3);
        int b2 = 10;
        i4 = b2++;
        System.out.println("b2 = " + b2 + ",i4 = " + i4);
    }
}
