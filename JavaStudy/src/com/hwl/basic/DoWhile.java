package com.hwl.basic;

/**
 * @author 何文亮
 * @date 2021-01-11
 */
public class DoWhile {
    public static void main(String[] args) {
        int num = 1;
        int sum = 0,count =0;
        do {
            if(num % 2 == 0){
                System.out.println(num);
                sum += sum;
                ++count;
            }
            num++;
        } while (num < 100);
    }
}
