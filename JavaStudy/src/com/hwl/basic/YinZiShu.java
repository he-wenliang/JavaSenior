package com.hwl.basic;

/**
 * @author 何文亮
 * @date 2021-01-12
 */
public class YinZiShu {
    public static void main(String[] args) {
        int sum = 0;
        for (int i = 1; i <= 1000; i++) {
            for (int j = 1; j <= i / 2; j++) {
                if (i % j == 0) {
                    sum += j;
                }
            }
            if (i == sum) {
                System.out.println(i);
            }
            sum = 0;
        }
    }
}
