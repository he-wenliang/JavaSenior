package com.hewl.p2.view;

import com.hewl.p2.bean.Customer;
import com.hewl.p2.service.CustomerList;
import com.hewl.p2.util.CMUtility;

public class CustomerView {

	private CustomerList customerList = new CustomerList(10);

	public CustomerView() {
		Customer customer = new Customer("Tom", '男', 23, "15809659229", "tom@gmail.com");
		customerList.addCustomer(customer);
	}

	public static void main(String[] args) {
		CustomerView view = new CustomerView();
		view.enterMainMenu();
	}

	/**
	 * 
	 * @Description 主界面
	 * @author 何文亮
	 * @date 2021年1月25日下午6:06:30
	 */
	public void enterMainMenu() {
		boolean isFlag = true;
		while (isFlag) {
			System.out.println("-------------------客户信息管理软件-------------------");
			System.out.println("                    1    添加客户");
			System.out.println("                    2    修改客户 ");
			System.out.println("                    3    删除客户 ");
			System.out.println("                    4    客户列表 ");
			System.out.println("                    5    退        出 \n");
			System.out.println("  请选择操作（1-5）： ");

			char select = CMUtility.readMenuSelection();
			switch (select) {
			case '1':
				addNewCustomer();
				break;
			case '2':
				modifyCustomer();
				break;
			case '3':
				deleteCustomer();
				break;
			case '4':
				listAllCustomer();
				break;
			case '5':
				System.out.print("确认退出（Y/N）？");
				char confirmExit = CMUtility.readConfirmSelection();
				if (confirmExit == 'Y') {
					isFlag = false;
				}
			}
		}
	}

	/**
	 * 
	 * @Description 添加客户操作
	 * @author 何文亮
	 * @date 2021年1月25日下午6:06:42
	 */
	public void addNewCustomer() {

		System.out.println("-----------------添加客户-----------------");
		System.out.println("客户姓名：");
		String name = CMUtility.readString(10);
		System.out.println("客户性别：");
		char gender = CMUtility.readChar();
		System.out.println("客户年龄：");
		int age = CMUtility.readInt();
		System.out.println("客户电话：");
		String tel = CMUtility.readString(11);
		System.out.println("客户邮箱：");
		String email = CMUtility.readString(15);
		Customer cust = new Customer(name, gender, age, tel, email);

		boolean isSuccess = customerList.addCustomer(cust);
		if (isSuccess) {
			System.out.println("-----------------添加成功-----------------\t");
		} else {
			System.out.println("-----------客户列表已满，添加失败-------------\t");
		}
	}

	/**
	 * 
	 * @Description 修改客户操作
	 * @author 何文亮
	 * @date 2021年1月25日下午6:06:46
	 */
	public void modifyCustomer() {
		System.out.println("-----------------修改客户信息-----------------");
		int index;
		Customer cust;
		for (;;) {
			System.out.println("请输入要修改的客户序号(输入-1退出)");
			index = CMUtility.readInt();
			if (index == -1) {
				return;
			}

			cust = customerList.getCustomer(index - 1);
			if (cust == null) {
				System.out.println("无法找到客户!");
			} else {
				break;
			}
		}

		System.out.print("姓名（" + cust.getName() + "）：");
		String name = CMUtility.readString(10, cust.getName());
		System.out.print("性别（" + cust.getGender() + "）：");
		char gender = CMUtility.readChar(cust.getGender());
		System.out.print("年龄（" + cust.getAge() + "）：");
		int age = CMUtility.readInt(cust.getAge());
		System.out.print("电话（" + cust.getPhone() + "）：");
		String tel = CMUtility.readString(11, cust.getPhone());
		System.out.print("邮箱（" + cust.getEmail() + "）：");
		String email = CMUtility.readString(30, cust.getEmail());

		Customer newCust = new Customer(name, gender, age, tel, email);
		boolean isReplaced = customerList.repaceCustomer(index - 1, newCust);
		if (isReplaced) {
			System.out.println("-----------------修   改  成  功-----------------\t");
		} else {
			System.out.println("-----------------修   改  失  败-----------------\t");
		}
	}

	/**
	 * 
	 * @Description 删除客户操作
	 * @author 何文亮
	 * @date 2021年1月25日下午6:06:49
	 */
	public void deleteCustomer() {
		System.out.println("-----------------删除客户-----------------");
		Customer cust;
		int index;
		for (;;) {
			System.out.println("请输入要删除的客户序号(输入-1退出)");
			index = CMUtility.readInt();
			if (index == -1) {
				return;
			}

			cust = customerList.getCustomer(index - 1);
			if (cust == null) {
				System.out.println("无法找到客户!");
			} else {
				break;
			}
		}
		System.out.print("是否确认删除（Y/N）:");
		char isDelete = CMUtility.readConfirmSelection();
		if (isDelete == 'Y') {
			boolean deleteSuccess = customerList.deleteCustomer(index - 1);
			if (deleteSuccess) {
				System.out.println("-----------------删  除  成  功-----------------\t");
			} else {
				System.out.println("删除失败，请输入正确序号:1->" + customerList.getTotal());
			}
		}
	}

	/**
	 * 
	 * @Description 显示列表客户操作
	 * @author 何文亮
	 * @date 2021年1月25日下午6:06:52
	 */
	public void listAllCustomer() {
		System.out.println("---------------------客  户  列  表--------------------\n");
		int total = customerList.getTotal();
		if (total == 0) {
			System.out.println("没有客户信息！");
		} else {
			System.out.println("编号\t姓名\t性别\t年龄\t电话\t\t邮箱");
			Customer[] custList = customerList.getAllCustomer();
			for (int i = 0; i < total; i++) {
				System.out.println(i + 1 + "\t" + custList[i].getName() + "\t" + custList[i].getGender() + "\t"
						+ custList[i].getAge() + "\t" + custList[i].getPhone() + "\t" + custList[i].getEmail() + "\t");
			}
		}
		System.out.println("---------------------客户列表完成-------------------\n");
	}
}
