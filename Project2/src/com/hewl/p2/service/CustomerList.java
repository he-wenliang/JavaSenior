package com.hewl.p2.service;

import com.hewl.p2.bean.Customer;

public class CustomerList {

	private Customer[] customers;
	private int total = 0;

	/**
	 * 初始化数组
	 * 
	 * @param totalCustomer
	 */
	public CustomerList(int totalCustomer) {
		customers = new Customer[totalCustomer];
	}

	/**
	 * 
	 * @Description 将指定的客户添加到数组中
	 * @author 何文亮
	 * @date 2021年1月25日下午5:40:14
	 * @param customer
	 * @return true：添加成功，false：添加失败；
	 */
	public boolean addCustomer(Customer customer) {
		if (total >= customers.length) {
			return false;
		}
//		customers[total] = customer;
//		total++;
		customers[total++] = customer;
		return true;
	}

	/**
	 * 
	 * @Description 修改指定索引的客户信息
	 * @author 何文亮
	 * @date 2021年1月25日下午5:44:05
	 * @param index
	 * @param cust
	 * @return true：修改成功，false：修改失败；
	 */
	public boolean repaceCustomer(int index, Customer cust) {
		if (index < 0 || index >= total) {
			return false;
		}
		customers[index] = cust;
		return true;
	}

	/**
	 * 
	 * @Description
	 * @author 何文亮
	 * @date 2021年1月25日下午5:46:51
	 * @param index
	 * @param cust
	 * @return true：删除成功，false：删除失败；
	 */
	public boolean deleteCustomer(int index) {
		if (index < 0 || index >= total) {
			return false;
		}
		for (int i = index; i < total - 1; i++) {
			customers[i] = customers[i + 1];
		}
		// 最后一个元素置空
//		customers[total - 1] = null;
//		total--;
		customers[--total] = null;
		return true;

	}

	/**
	 * 
	 * @Description 获取所有客户信息
	 * @author 何文亮
	 * @date 2021年1月25日下午5:46:57
	 * @param index
	 * @return
	 */
	public Customer[] getAllCustomer() {
		Customer[] custs = new Customer[total];
		for (int i = 0; i < total; i++) {
			custs[i] = customers[i];
		}
		return custs;
	}

	/**
	 * 
	 * @Description 获取指定位置客户
	 * @author 何文亮
	 * @date 2021年1月25日下午5:59:50
	 * @param index
	 * @return
	 */
	public Customer getCustomer(int index) {
		if (index < 0 || index >= total) {
			return null;
		}
		return customers[index];
	}
	/**
	 * 
	 * @Description 获取客户数量
	 * @author 何文亮
	 * @date 2021年1月25日下午6:01:21
	 * @return
	 */
	public int getTotal() {
		return total;
	}
}
